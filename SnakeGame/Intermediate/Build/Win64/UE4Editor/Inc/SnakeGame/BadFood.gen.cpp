// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/BadFood.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBadFood() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABadFood_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABadFood();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ABadFood::StaticRegisterNativesABadFood()
	{
	}
	UClass* Z_Construct_UClass_ABadFood_NoRegister()
	{
		return ABadFood::StaticClass();
	}
	struct Z_Construct_UClass_ABadFood_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABadFood_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABadFood_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BadFood.h" },
		{ "ModuleRelativePath", "BadFood.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABadFood_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABadFood, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABadFood_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABadFood>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABadFood_Statics::ClassParams = {
		&ABadFood::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABadFood_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABadFood_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABadFood()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABadFood_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABadFood, 3238128970);
	template<> SNAKEGAME_API UClass* StaticClass<ABadFood>()
	{
		return ABadFood::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABadFood(Z_Construct_UClass_ABadFood, &ABadFood::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABadFood"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABadFood);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
