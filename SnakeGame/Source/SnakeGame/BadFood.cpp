// Fill out your copyright notice in the Description page of Project Settings.


#include "BadFood.h"
#include "Snake.h"

// Sets default values
ABadFood::ABadFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABadFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABadFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABadFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			Snake->SetActorTickInterval(0.8);
			this->Destroy();
		}
	}
}



